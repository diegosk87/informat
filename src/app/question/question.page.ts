import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-question',
  templateUrl: './question.page.html',
  styleUrls: ['./question.page.scss'],
})
export class QuestionPage implements OnInit {
  email = '';
  message = '';
  user = '';

  constructor(
    private dataService:DataService,
    private toastCtrl:ToastController  
  ) { }

  ngOnInit() {

  }

  saveMessage() {
    var data = {
      email: this.email,
      message: this.message,
      user: this.user
    }

    this.dataService.sendMessage(data).subscribe(
      response => {
        this.showToast(response['message']);
      },
      err => {
        console.log(err);
      }

    )
  }

  async showToast(msg:string) {
    const toast = await this.toastCtrl.create({
      message: msg,
      duration: 1000
    });
    toast.present();
  }

}
