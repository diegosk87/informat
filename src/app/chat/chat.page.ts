import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {
  messages:Array<any> = new Array<any>();
  message:any;
  user:String = '';

  constructor() { }

  ngOnInit() {
    this.message = {
      name: 'AT-Bot',
      message: 'Hola, soy AT-Bot, como puedo ayudarte?'
    };
    
    this.messages.push(this.message);
  }

  send() {
    this.message = {
      name: 'Estimado ciudadano',
      message: `${this.user}`
    };

    this.messages.push(this.message);

    setTimeout(() => {
      this.message = {
        name: 'AT-Bot',
        message: 'Podrias revisar los datos de Yautepec y Mazatepec'
      };
  
      this.messages.push(this.message);
    }, 1000);
  }

}
