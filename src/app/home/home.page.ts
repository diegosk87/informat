import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HereService } from '../services/here.service';
import { DataService } from '../services/data.service';

declare var H: any;

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  name:String = "";
  map:any;

  data:Array<any> = new Array<any>();

  constructor(
    private router:Router,
    private hereService:HereService,
    private dataService:DataService
    ) {}

  ngOnInit() {

    this.dataService.getData().subscribe(
      response => {
        this.data.push(response['docs']);
        this.createMap();
      },
      err => {
        console.log(err);
      }
    );
  }

  createMap() {
    var platform = new H.service.Platform({
      'apikey': 'JcHsKC16nBbr6S7Uaga87oWtqbcpxN8VkiNgBGr2HyA'
    });
    
    var defaultLayers = platform.createDefaultLayers();
    
    this.map = new H.Map(document.getElementById('map'),
        defaultLayers.vector.normal.map,{
        center: {lat:18.84546, lng:-99.22377},
        zoom: 9,
        pixelRatio: window.devicePixelRatio || 1
    });
    
    window.addEventListener('resize', () => this.map.getViewPort().resize());
    
    var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(this.map));
    
    var ui = H.ui.UI.createDefault(this.map, defaultLayers, 'es-ES');

    this.data[0].forEach((da) => {
      var mark = new H.map.Marker({
        lat:da.Latitude, lng:da.Longitude
      });

      this.map.addObject(mark);
  
      mark.addEventListener('tap', () => {
        this.router.navigate(['/detail'], {
          queryParams: da
        });
      }, false);
    });
  }

  searchPlace() {
    this.hereService.getCoordsOfMunicipality(this.name).subscribe(
      response => {
        var coords = response['Response'].View[0].Result[0].Location.DisplayPosition;
            this.map.setCenter({
              lat: coords.Latitude,
              lng: coords.Longitude
            });

            this.map.setZoom(16);
      },
      err => {
        console.log(err);
      }
    );
  }
}
