import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.page.html',
  styleUrls: ['./detail.page.scss'],
})
export class DetailPage implements OnInit {
  data:any;

  category = [
    'Ejercicios',
    'Actos Juridicos',
    'Nombres de titulares',
    'Montos por obras',
    'Areas responsables',
  ]

  constructor(private activatedRoute:ActivatedRoute) { }

  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  public barChartType = 'bar';
  public barChartLegend = true;
  public barChartData = [
    {data: [65, 59, 80, 81, 56, 55, 40], label: 'Ingresos'},
    {data: [28, 48, 40, 19, 86, 27, 90], label: 'Egresos'}
  ];

  public doughnutChartLabels = ['Obras Publicas', 'Gastos Internos'];
  public doughnutChartData = [120, 90];
  public doughnutChartType = 'doughnut';

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(
      res => {
        this.data = res
      }
    );
  }

}
