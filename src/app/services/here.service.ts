import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HereService {

  url:String = 'https://geocoder.api.here.com/6.2/geocode.json?app_id=5SzZvNIuX9hhOu1M3VGZ&app_code=VF7qI6l-p9mcRpUG1VillA&searchtext=';

  constructor(private httpClient:HttpClient) { }

  public getCoordsOfMunicipality(name:String) {
    return this.httpClient.get(`${this.url}${name}`);
  }
}
